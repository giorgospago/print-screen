const fs = require("fs");
const request = require('request');
const rp = require('request-promise');
const cheerio = require('cheerio');

function download(uri, filename){
	request.head(uri, function(err, res, body){
		request(uri).pipe(fs.createWriteStream(filename));
	});
}

function checkImage(url){

	const options = {
		method: 'POST',
		url: 'https://cxl-services.appspot.com/proxy',
		qs: { url: 'https://vision.googleapis.com/v1/images:annotate' },
		headers: {
			'Cache-Control': 'no-cache',
			origin: 'https://cloud.google.com',
			authority: 'https://cloud.google.com/vision/',
			'User-Agent': 'stagefright/1.2 (Linux;Android 5.0)',
			Referer: 'https://cloud.google.com/vision/',
			'Content-Type': 'application/json'
		},
		body:{ 
			requests:[ 
				{
					image: {
						source: {
							imageUri: url
						}
					},
					features: [ { type: 'SAFE_SEARCH_DETECTION', maxResults: 50 } ],
					imageContext: {
						cropHintsParams: { 
							aspectRatios: [ 0.8, 1, 1.2 ]
						}
					}
				}
			]
		},
		json: true
	};

	request(options, function (error, response, body) {
		if(
			body &&
			body.responses &&
			body.responses.length > 0 &&
			body.responses[0] &&
			body.responses[0].safeSearchAnnotation
		){
			var res = body.responses[0].safeSearchAnnotation;
			var validResponses = ['VERY_LIKELY','LIKELY'];

			if(validResponses.indexOf(res.adult) >= 0){
				console.log(url);
				const name = url.split('/').pop();
				download(url, 'racy/'+name);
			}
		}
		else{
			console.log(url);
			const name = url.split('/').pop();
			download(url, 'images/'+name);
		}
	});

}

async function getImage(){
	const hash = genHash();
	const options = {
		url: `https://prnt.sc/${hash}`,
		headers: {
			"Referer": `https://prnt.sc/${hash}`,
			"User-Agent": "stagefright/1.2 (Linux;Android 5.0)"
		}
	};
	const $ = cheerio.load(await rp(options));
	const img = $("meta[property='og:image']").attr('content');

	if(!img || img.indexOf('//st.prntscr.com') != -1){
		getImage();
		return;
	}
	//checkImage(img);
	console.log(img);
	const name = img.split('/').pop();
	download(img, 'images/'+name);
	
	getImage();
}

function genHash(){
	var text = "";
	var possible = "abcdefghijklmnopqrstuvwxyz0123456789";

	for (var i = 0; i < 6; i++)
		text += possible.charAt(Math.floor(Math.random() * possible.length));
	return text;
}

getImage();
